FROM node:11

MAINTAINER UK Web Communications

WORKDIR /app
ENV HOME /app


  
COPY ./package.json /app/
COPY ./gulpfile.js /app/
RUN npm install -g gulp@3
RUN yarn

COPY ./docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
